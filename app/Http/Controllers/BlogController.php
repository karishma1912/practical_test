<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class BlogController extends Controller
{
    public function index(){
    	$blogs = User::with('Blog')->where('id',Auth::id())->get()->toArray();

    	return view('blogs.index',compact('blogs'));
    }
}
