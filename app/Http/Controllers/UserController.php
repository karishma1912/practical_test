<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UsersRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Blog;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $relationship = User::with('Blog')->get()->toArray();
        // dd($relationship);
        $users = User::get()->toArray();
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'        => 'required|alpha|max:32',

            'last_name'         => 'required|alpha|max:32',
            'username'         => 'required|alpha|max:32',
            'password'          => 'required|min:6',
            'confirm_password'  => 'required|same:password', 

            'contact_no'        => 'required|numeric|digits_between:6,18|regex:/^[0-9]{1,10}$/',

            'email'             => 'required|email|max:100|unique:users,email,NULL,id,deleted_at,NULL',
            'gender'            => 'required',
        ]);
        $user_data = $request->all();
        $user = new User();
        $user->first_name = $user_data['first_name'];
        $user->last_name = $user_data['last_name'];
        $user->username = $user_data['username'];
        $user->password = \Hash::make($user_data['password']);
        $user->contact_no = $user_data['contact_no'];
        $user->email = $user_data['email'];
        $user->gender = $user_data['gender'];
        $user->dob = date('Y-m-d',strtotime($user_data['dob']));
        $user->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(user $user)
    {
        $user['dob'] = date('d-m-Y',strtotime($user['dob']));
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user $user)
    {
        $id = \Request::segment(3);                                                                             
        $this->validate($request, [
            'first_name'        => 'required|alpha|max:32',

            'last_name'         => 'required|alpha|max:32',
            'username'         => 'required|alpha|max:32',

            'contact_no'        => 'required|numeric|digits_between:6,18|regex:/^[0-9]{1,10}$/',

            'email' => 'required|email|max:100|unique:users,email,{$id},id,deleted_at,NULL',
            'gender'            => 'required',
        ]);
        $user_data = $request->all();
        $user = User::find(\Request::segment(3));
        $user->first_name = $user_data['first_name'];
        $user->last_name = $user_data['last_name'];
        $user->username = $user_data['username'];
        $user->contact_no = $user_data['contact_no'];
        $user->email = $user_data['email'];
        $user->gender = $user_data['gender'];
        $user->dob = date('Y-m-d',strtotime($user_data['dob']));
        $user->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user  $user
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user_data  = $request->all();


        if($user_data['id'] == \Auth::id()){
            return "error";
        }else{
            $user_delete = User::find($user_data['id'])->delete();
            return "success";
        }
    }
}
