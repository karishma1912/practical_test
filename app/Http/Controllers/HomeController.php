<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_count =  User::where('deleted_at',NULL)->count();
        $blog_count =  Blog::where('user_id',\Auth::id())->count();

        return view('home',compact('user_count','blog_count'));
    }
}
