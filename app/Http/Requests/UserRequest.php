<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
// use Illuminate\Http\Request;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) 
    {
        $input = $request->all();
        $id = \Request::segment(3);
        
        switch ($this->method()) {

            case 'POST':
            {
                $rules = [
                    'first_name'        => 'required|alpha|max:32',

                    'last_name'         => 'required|alpha|max:32',
                    'user_name'         => 'required|alpha|max:32',
                    'password'          => 'required|min:6',
                    'confirm_password'  => 'required|same:password' 

                    'contact_no'        => 'required|numeric|digits:10|regex:/^[0-9]{1,10}$/',

                    'email'             => 'required|email|max:100|unique:users',
                    'gender'            => 'required',
                ]; 
                return $rules;
            }
            case 'PUT':
            {
                $rules = [
                    'first_name'        => 'required|alpha|max:32',

                    'last_name'         => 'required|alpha|max:32',
                    'user_name'         => 'required|alpha|max:32',
                    'password'          => 'required|min:6',
                    'confirm_password'  => 'required|same:password' 

                    'contact_no' => 'required|numeric|digits:10|regex:/^[0-9]{1,10}$/',

                    'email' => 'required|email|max:100|unique:users,email,'.\Request::segment(3),
                    'gender'         => 'required',
                ];
                return $rules;
            }
            default : break;
        }
    }

}
