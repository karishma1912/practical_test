<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(array('middleware' => 'auth'),function(){
	Route::post('users/delete', 'UserController@delete')->name('users.delete');
	Route::post('/users/update/{id}', 'UserController@update')->name('users.update');
	Route::resource('/users', 'UserController',['except'=>['update','delete']]);

	Route::get('/blog', 'BlogController@index')->name('blog.index');

});
