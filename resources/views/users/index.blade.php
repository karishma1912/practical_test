<!DOCTYPE html>
<html lang="en">
<head>
  <title>Practical Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Users List
    <a href="{{route('users.create')}}" class="btn btn-info btn-lg" style="font-size: 12px;font-weight: bold;float: right;" >
      <span class="glyphicon glyphicon-plus"></span> Add 
    </a>
    <a href="{{route('home')}}" class="btn btn-info btn-lg" style="font-size: 12px;font-weight: bold;float: right;margin-bottom: 10px;margin-right: 10px" >
      <span class="glyphicon glyphicon-arrow-left"></span> Back 
    </a>
  </h2>   
  <table class="table table-responsive">
    <thead>
      <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>User Name</th>
        <th>Email</th>
        <th>Contact Number</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $value)
        <tr>
          <td>{{$value['first_name']}}</td>
          <td>{{$value['last_name']}}</td>
          <td>{{$value['username']}}</td>
          <td>{{$value['email']}}</td>
          <td>{{$value['contact_no']}}</td>
          <td>
            <p>
              <a href="{{route('users.edit',$value['id'])}}" class="btn btn-info btn-lg" style="font-size: 12px;
    font-weight: bold;">
                <span class="glyphicon glyphicon-pencil"></span> Edit 
              </a>
              <a href="javascript:;" class="btn btn-info btn-lg delete" style="font-size: 12px;font-weight: bold;" data-id="{{$value['id']}}">
                <span class="glyphicon glyphicon-trash"></span> Delete 
              </a>
            </p> 
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
<script type="text/javascript">
  $('.delete').click(function(){
    console.log('hi');
    var result = confirm("Do you want to delete?");
    var id = $(this).attr('data-id');
    console.log(id);
    if (result) {
      $.ajax({
        type: 'POST',
        url: "{{route('users.delete')}}",
        data: {'id':id,'_token':"{{csrf_token()}}"},
        success: function (data) 
        {
            if(data == "success"){
              toastr.success("User has been deleted successfully");
            }else{
              toastr.error("Logged in User can not be delete");
            }
            setTimeout(function(){ 
              window.location.href = "{{route('users.index')}}" 
            }, 1000);
        },
      });
    }      
  })
</script>
</body>
</html>
