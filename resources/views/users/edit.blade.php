<!DOCTYPE html>
<html lang="en">
<head>
  <title>Practical Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{asset('css/dataranger.css')}}">
  <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <script src="{{asset('js/daterangepicker.min.js')}}"></script>
  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/dateranger.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>
<body>

<div class="container">
  <h2>User Edit
    <a href="{{route('users.index')}}" class="btn btn-info btn-lg" style="font-size: 12px;font-weight: bold;float: right;margin-bottom: 10px" >
      <span class="glyphicon glyphicon-arrow-left"></span> Back 
    </a>
  </h2>
  <form class="form-horizontal" action="{{route('users.store')}}" method="post" id="user_form">
  <input type="hidden" name="id" value="{{$user['id']}}" id="id">
    <div class="form-group" id="first_name_error_div">
      <label class="control-label col-sm-2" for="first_name">First Name:</label>
      <div class="col-sm-10">
        <input type="first_name" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" value="{{$user['first_name']}}">
        <span id="first_name_error"><?=$errors->first('first_name')?></span>
      </div>
    </div>
    <div class="form-group" id="last_name_error_div">
      <label class="control-label col-sm-2" for="last_name">Last Name:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name" value="{{$user['last_name']}}">
        <span id="last_name_error"><?=$errors->first('last_name')?></span>

      </div>
    </div>
    <div class="form-group" id="user_name_error_div">
      <label class="control-label col-sm-2" for="user_name">User Name:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="user_name" placeholder="Enter User Name" name="user_name" value="{{$user['user_name']}}">
        <span id="user_name_error"><?=$errors->first('user_name')?></span>

      </div>
    </div>
    <div class="form-group" id="email_error_div">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="email" placeholder="Enter Email" name="email" value="{{$user['email']}}">
        <span id="email_error"><?=$errors->first('email')?></span>

      </div>
    </div>
    <div class="form-group" id="gender_error_div">
      <label class="control-label col-sm-2" for="gender">Gender:</label>
      <div class="col-sm-10">          
        <select class="form-control" name="gender">
          <option value="">Select Gender</option>
          @foreach(config('project.gender') as $value)
          <option value="{{$value}}" @if($value == $user['gender']) selected="true" @endif>{{ucfirst($value)}}</option>
          @endforeach
        </select>  
        <span id="gender_error"><?=$errors->first('gender')?></span>

      </div>
    </div>
    <div class="form-group" id="contact_no_error_div">
      <label class="control-label col-sm-2" for="contact_no">Contact Number:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="contact_no" placeholder="Enter Contact Number" name="contact_no" value="{{$user['contact_no']}}">
        <span id="contact_no_error"><?=$errors->first('contact_no')?></span>

      </div>
    </div>
    <div class="form-group" id="dob_error_div">
      <label class="control-label col-sm-2" for="dob">Date Of Birth:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="dob" name="dob" value="{{$user['dob']}}">
        <span id="dob_error"><?=$errors->first('dob')?></span>

      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Update</button>
      </div>
    </div>
  </form>
</div>

</body>
<script type="text/javascript">
  $('input[name="dob"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    "locale": {
        "format": "DD-MM-YYYY",
    },
    maxDate: new Date()
  });
  $("#user_form").on('submit', function (e) {
    var id = $('#id').val();
    e.preventDefault();
    var url1 = "<?=URL::route('users.update', ['id' => ':id'])?>";
    var path = url1.replace(':id',id);
    $.ajax({
        type: 'POST',
        url: path,
        data: new FormData($("#user_form")[0]),
        async:true,
        processData: false,
        contentType: false,
        success: function (data) 
        {
            toastr.success("User has been updated successfully");
            setTimeout(function(){ 
              window.location.href = "{{route('users.index')}}" 
            }, 1000);
        },
        error: function (response) 
        {
          $("[id$='_error']").empty();
          $("[id$='_error_div']").removeClass('text-danger');

            $.each(response.responseJSON.errors, function(k,v){
              $('#'+k+'_error').text(v);
              $('#'+k+'_error_div').addClass('text-danger');

          });

        }
    })
});
</script>
  
</html>
