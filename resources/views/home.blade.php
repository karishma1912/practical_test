@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="container bootstrap snippet">
                        <div class="row">
                            <a href="{{route('users.index')}}">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="mini-stat clearfix bg-facebook rounded">
                                        <span class="mini-stat-icon"><i class="fa fa-user"></i></span>
                                        <div class="mini-stat-info">
                                            <span>{{$user_count}}</span>
                                            Users
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a href="{{route('blog.index')}}">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="mini-stat clearfix bg-twitter rounded">
                                        <span class="mini-stat-icon"><i class="fa fa-user"></i></span>
                                        <div class="mini-stat-info">
                                            <span>{{$blog_count}}</span>
                                            Blog
                                        </div>
                                    </div>
                                </div>  
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
