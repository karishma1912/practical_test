<!DOCTYPE html>
<html lang="en">
<head>
  <title>Practical Test</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Blogs List
    <a href="{{route('home')}}" class="btn btn-info btn-lg" style="font-size: 12px;font-weight: bold;float: right;margin-bottom: 10px;margin-right: 10px" >
      <span class="glyphicon glyphicon-arrow-left"></span> Back 
    </a>
  </h2>   
  <table class="table table-responsive">
    <thead>
      <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Slug</th>
      </tr>
    </thead>
    <tbody>
      @if(count($blogs[0]['blog']) > 0)
        @foreach($blogs[0]['blog'] as $value)
          <tr>
            <td>{{$value['title']}}</td>
            <td>{{$value['description']}}</td>
            <td>{{$value['slug']}}</td>
          </tr>
        @endforeach
      @else
        <tr><td>There were no record found!</td></tr>  
      @endif
    </tbody>
  </table>
</div>
</body>
</html>
